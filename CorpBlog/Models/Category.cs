﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CorpBlog.Models {
    public class Category {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public List<Post> Posts { get; set; }
    }
}
