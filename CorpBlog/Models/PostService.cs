﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CorpBlog.Data;
using Microsoft.EntityFrameworkCore;

namespace CorpBlog.Models {
    public class PostService : IPostService {
        public PostService(CorpBlogDbContext db) {
            this.db = db;
        }

        public IEnumerable<Post> AllPosts() {
            return (from p in db.Posts
                    orderby p.CreatedAt descending
                    select p).Include("Category");
        }

        public IEnumerable<Post> PublishedPosts() {
            return from p in db.Posts
                   where p.Published == true
                   orderby p.CreatedAt descending
                   select p;
        }

        public int PostsCount() {
            return (from p in db.Posts
                   orderby p.CreatedAt descending
                   select p).Count();
        }

        public CorpBlogDbContext Db { get => db; }

        private readonly CorpBlogDbContext db;
    }
}
