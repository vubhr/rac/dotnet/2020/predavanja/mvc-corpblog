﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CorpBlog.Models {
    public class Post {
        /* naslov, sadržaj, datum izrade te mogućnost definiranja da li je pojedina vijest objavljena */
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Naslov")]
        public string Title { get; set; }

        [Display(Name = "Sadržaj")]
        public string Content { get; set; }

        public string Author { get; set; }

        public DateTime CreatedAt { get; set; }

        public bool Published { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
