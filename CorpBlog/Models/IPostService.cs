﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CorpBlog.Data;

namespace CorpBlog.Models {
    public interface IPostService {
        IEnumerable<Post> AllPosts();
        IEnumerable<Post> PublishedPosts();
        int PostsCount();

        CorpBlogDbContext Db { get; }
    }
}
